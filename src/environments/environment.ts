// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC6jlYWfMo7ot1rP4xtnySe-GVJf7f1b20",
    authDomain: "patlijan-74119.firebaseapp.com",
    databaseURL: "https://patlijan-74119.firebaseio.com",
    projectId: "patlijan-74119",
    storageBucket: "patlijan-74119.appspot.com",
    messagingSenderId: "997067994284",
    appId: "1:997067994284:web:a757dbf8662fbbc42b1b09",
    measurementId: "G-895F8BFRQL"
  },
  paymentUrl: 'http://localhost:8080/create-payment-intent'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
