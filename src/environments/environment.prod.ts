export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: "AIzaSyC6jlYWfMo7ot1rP4xtnySe-GVJf7f1b20",
    authDomain: "patlijan-74119.firebaseapp.com",
    databaseURL: "https://patlijan-74119.firebaseio.com",
    projectId: "patlijan-74119",
    storageBucket: "patlijan-74119.appspot.com",
    messagingSenderId: "997067994284",
    appId: "1:997067994284:web:a757dbf8662fbbc42b1b09",
    measurementId: "G-895F8BFRQL"
  },
  paymentUrl: 'https://mighty-stream-59437.herokuapp.com/create-payment-intent'
};
