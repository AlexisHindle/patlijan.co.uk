import { TestBed } from '@angular/core/testing';

import { FirebaseOrdersService } from './firebase-orders.service';

describe('FirebaseOrdersService', () => {
  let service: FirebaseOrdersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseOrdersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
