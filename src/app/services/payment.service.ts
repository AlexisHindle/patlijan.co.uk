import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  client_secret: any;
  constructor(
    public http: HttpClient
  ) { }

  public createIntent(amount, email) {
    return this.http.post<any>( environment.paymentUrl , {
      amount: amount,
      email: email
    }).pipe(
      map((res: any) => {
        return res;
      })
    )
  }
}
