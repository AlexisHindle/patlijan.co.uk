import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Product, OrderNumber } from '../products.model';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class FirebaseProductsService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  public getProducts() {
    return this.firestore.collection('products').snapshotChanges();
  }

  public getSingleProduct(id) {
    return this.firestore.collection('products').doc(id).valueChanges();
  }

  public createProduct(product: Product) {
    return this.firestore.collection('products').add(product)
  }

  public updateProduct(product: Product) {
    this.firestore.doc('products/' + product.id).set(product, {merge: true});
  }

  public deletePolicy(productId: number){
    this.firestore.doc('products/' + productId).delete();
  }

  // TODO: Look at creating new service
  public getOrderNumber() {
    return this.firestore.collection('orderNumber').snapshotChanges();
  }

  public increaseCount(number) {
    const increment = firebase.firestore.FieldValue.increment(1);
    this.firestore.collection('orderNumber').doc(number[0].id).update({ orderNumber: increment });

  }
}
