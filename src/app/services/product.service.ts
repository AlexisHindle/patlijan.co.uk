import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: any;
  constructor(
    private http: HttpClient
  ) {}

  load() :Promise<any>  {
    const promise = this.http.get('../assets/data/products.json')
      .toPromise()
      .then(data => {
        Object.assign(this, data);
        return data;
      });
    return promise;
}

  public getProducts() {
    return this.http.get('../assets/data/products.json')
    .pipe(map((response: any) => {
      this.products = response;
    }))
  }
}
