import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseOrdersService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  public getOrders() {
    return this.firestore.collection('orders').snapshotChanges();
  }

  // TODO: Need to look at adding order to products model
  createOrder(order) {
    return this.firestore.collection('orders').add(order)
  }

  public updateOrder(order) {
    this.firestore.doc('orders/' + order.id).set(order, {merge: true});
  }
  
}
