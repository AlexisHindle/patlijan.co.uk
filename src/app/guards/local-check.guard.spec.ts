import { TestBed } from '@angular/core/testing';

import { LocalCheckGuard } from './local-check.guard';

describe('LocalCheckGuard', () => {
  let guard: LocalCheckGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LocalCheckGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
