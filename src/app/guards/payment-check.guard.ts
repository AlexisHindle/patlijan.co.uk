import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentCheckGuard implements CanActivate {
  constructor(public router: Router){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const localProducts = JSON.parse(localStorage.getItem('localProducts'));
    let total = 0;
    if (localProducts) {
        const filteredItems = localProducts.filter(x => x.numberOfItems > 0);
        const arrayOfPrices = [];
        filteredItems.forEach(element => {
          arrayOfPrices.push(element.price * element.numberOfItems);
        });
        arrayOfPrices.forEach(item => {
          total = total + item;
          return total;
        });
    } else {
      this.router.navigate(['']);
      return false
    }

    if (total) {
      return true;
    }

    this.router.navigate(['']);
    return false;
  }
  
}
