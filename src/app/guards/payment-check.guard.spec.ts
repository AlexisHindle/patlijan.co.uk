import { TestBed } from '@angular/core/testing';

import { PaymentCheckGuard } from './payment-check.guard';

describe('PaymentCheckGuard', () => {
  let guard: PaymentCheckGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PaymentCheckGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
