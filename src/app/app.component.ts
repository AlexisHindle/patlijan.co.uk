import { Component } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Patlijan';

  constructor(private titleService: Title, private metaService: Meta) {}
  
  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {name: 'keywords', content: 'Mediterranean-inspired food'},
      {name: 'description', content: 'Easy, healthy, home-delivered fresh food for the more adventurous foodies of West Berkshire. A choice of Mediterranean-inspired salads and of main courses to be reheated at home.'},
      {name: 'robots', content: 'index, follow'}
    ]);
  }
}
