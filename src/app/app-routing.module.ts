import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';

import { LocalCheckGuard } from "../app/guards/local-check.guard";
import { PaymentCheckGuard } from "../app/guards/payment-check.guard";
import { HomeComponent } from './pages/home/home.component';
import { BasketComponent } from './pages/basket/basket.component';
import { DeliveryInfoComponent } from './pages/delivery-info/delivery-info.component';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { LoginComponent } from './admin/login/login.component';
import { RegisterComponent } from './admin/register/register.component';
import { ForgotPasswordComponent } from './admin/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './admin/verify-email/verify-email.component';
import { StockComponent } from './admin/stock/stock.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { MenuItemsComponent } from './admin/menu-items/menu-items.component';
import { EditMenuItemsComponent } from './admin/edit-menu-items/edit-menu-items.component';
import { EditItemComponent } from './admin/edit-item/edit-item.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['admin/login']);

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'basket',
    component: BasketComponent
  },
  {
    path: 'delivery-info',
    component: DeliveryInfoComponent,
    canActivate: [PaymentCheckGuard]
  },
  {
    path: 'confirmation',
    component: ConfirmationComponent
  },
  {
    path: 'item-details/:id',
    component: ItemDetailsComponent,
    canActivate: [LocalCheckGuard]
  },
  {
    path:  'admin',
    children: [
        { 
          path:  'login',
          component:  LoginComponent
        },
        { 
          path:  'register', 
          component:  RegisterComponent 
        },
        { 
          path:  'forgot-password', 
          component:  ForgotPasswordComponent 
        },
        { 
          path:  'verify-email', 
          component:  VerifyEmailComponent 
        },
        {
          path: 'orders',
          component: OrdersComponent,
          canActivate: [AngularFireAuthGuard],
          data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        {
          path: 'stock',
          component: StockComponent,
          canActivate: [AngularFireAuthGuard],
          data: { authGuardPipe: redirectUnauthorizedToLogin } 
        },
        {
          path: 'menu-items',
          component: MenuItemsComponent,
          canActivate: [AngularFireAuthGuard],
          data: { authGuardPipe: redirectUnauthorizedToLogin }
        },
        {
          path: 'menu-edit',
          component: EditMenuItemsComponent,
          canActivate: [AngularFireAuthGuard],
          data: { authGuardPipe: redirectUnauthorizedToLogin }
        },
        {
          path: 'edit-item/:id',
          component: EditItemComponent,
          canActivate: [AngularFireAuthGuard],
          data: { authGuardPipe: redirectUnauthorizedToLogin }
        }
    ]
  },
  {
    path: '404',
    component: NotfoundComponent,
  },
  {
    path: '**', 
    component: NotfoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
