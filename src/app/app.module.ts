import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { MenuComponent } from './components/menu/menu.component';
import { HeroBannerComponent } from './components/hero-banner/hero-banner.component';
import { OrderComponent } from './pages/order/order.component';
import { BasketComponent } from './pages/basket/basket.component';
import { NotificationComponent } from './components/notification/notification.component';
import { BackgroundImageComponent } from './components/background-image/background-image.component';
import { FooterComponent } from './components/footer/footer.component';
import { DeliveryInfoComponent } from './pages/delivery-info/delivery-info.component';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { LoginComponent } from './admin/login/login.component';
import { RegisterComponent } from './admin/register/register.component';
import { ForgotPasswordComponent } from './admin/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './admin/verify-email/verify-email.component';
import { StockComponent } from './admin/stock/stock.component';
import { StickyHeaderDirective } from './directives/sticky-header.directive';
import { LoaderComponent } from './components/loader/loader.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { MenuItemsComponent } from './admin/menu-items/menu-items.component';
import { EditMenuItemsComponent } from './admin/edit-menu-items/edit-menu-items.component';
import { EditItemComponent } from './admin/edit-item/edit-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    MenuComponent,
    HeroBannerComponent,
    OrderComponent,
    BasketComponent,
    NotificationComponent,
    BackgroundImageComponent,
    FooterComponent,
    DeliveryInfoComponent,
    ConfirmationComponent,
    ItemDetailsComponent,
    PaymentComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    StockComponent,
    StickyHeaderDirective,
    LoaderComponent,
    NotfoundComponent,
    OrdersComponent,
    MenuItemsComponent,
    EditMenuItemsComponent,
    EditItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    FormsModule
  ],
  bootstrap: [
    AppComponent,
    
  ]
})
export class AppModule { }
