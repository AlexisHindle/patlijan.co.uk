export class Product {
    id: string;
    name: string;
    image: string;
    type: string;
    price: number;
    subtitle: string;
    description: string;
    itemsToBePurchased: Array<any>;
    product_id: number;
    numberOfItems: number;
    stock: number;
    details: {
        description: string;
        ingredients: string;
        arrives: string;
        store: string;
        life: string;
        serve: Array<string>;
        portions: string;
        recycle: string;
        allergens: Array<string>;
    }
}

export class OrderNumber {
    id: string;
    orderNumber: number
}
