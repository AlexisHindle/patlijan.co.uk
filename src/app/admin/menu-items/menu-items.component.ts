import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from "@angular/router";

import { FirebaseProductsService } from "../../services/firebase-products.service";
import { Product } from "../../products.model";


@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.scss']
})
export class MenuItemsComponent implements OnInit {
  public newItemForm: FormGroup;
  public products: any;
  public highestIdNumber: number
  constructor(
    private formBuilder: FormBuilder,
    public productService: FirebaseProductsService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(data => {
      this.products =  data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as Product;
      });
      let arrayOfId = []
      this.products.forEach(element => {
        arrayOfId.push(element.product_id)
      });
      this.highestIdNumber = Math.max(...arrayOfId);
    });
    
    this.newItemForm = this.formBuilder.group({
      product_id: [null, Validators.required],
      name: ['', Validators.required],
      subtitle: [''],
      description: [''],
      image: [''],
      type: [''],
      arrives: [''],
      ingredients: [''],
      life: [''],
      portions: [''],
      recycle: [''],
      serve: [''],
      store: [''],
      price: [''],
      available: [false]
    });
  }

  public onSubmit() {
    if (this.newItemForm.invalid) {
      console.log('Fill in the form');
      return;
    }
    else {
      const product = {
        name: this.newItemForm.value.name,
        image: this.newItemForm.value.image,
        type: this.newItemForm.value.type,
        price: this.newItemForm.value.price,
        subtitle: this.newItemForm.value.subtitle,
        description: this.newItemForm.value.description,
        itemsToBePurchased: [],
        numberOfItems: 0,
        product_id: this.newItemForm.value.product_id,
        stock: 0,
        details: {
            description: '',
            ingredients: this.newItemForm.value.ingredients,
            arrives: this.newItemForm.value.arrives,
            store: this.newItemForm.value.store,
            life: this.newItemForm.value.life,
            serve: this.newItemForm.value.serve,
            portions: this.newItemForm.value.portions,
            recycle: this.newItemForm.value.recycle,
            // allergens: [this.newItemForm.value.allergens],
        }
      }
      this.productService.createProduct(product as Product);
    }

  }

  public goToEdit() {
    this.router.navigate(['/admin/menu-edit']);
  }

}
