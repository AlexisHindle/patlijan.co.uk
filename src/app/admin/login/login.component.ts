import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  constructor(
    public  router:  Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
      // TODO: What extra custom validators are needed?
    })
  }

  get form() {
    return this.loginForm.controls;
  }

  public onLogin() {
    if (this.loginForm.invalid)
      return;
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password);      
  }


}
