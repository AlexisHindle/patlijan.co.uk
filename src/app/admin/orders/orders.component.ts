import { Component, OnInit } from '@angular/core';
import { FirebaseOrdersService } from '../../services/firebase-orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders: Array<any>;
  itemsOrdered: Array<any>;
  isCompleted: boolean = false;
  constructor(
    private firebaseOrdersService: FirebaseOrdersService
  ) { }

  ngOnInit(): void {
    this.firebaseOrdersService.getOrders().subscribe(data => {
      this.orders = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        };
      })

      this.orders.sort((a, b) => {
        return a.orderNumber - b.orderNumber;
      })

      this.orders.reverse();

      this.orders.forEach(x => {
        this.itemsOrdered = x.itemsOrdered;
      });
    });
  }
  
  public orderFulfilled(order) {
    order.completed = true;
    this.firebaseOrdersService.updateOrder(order)
    if (order.completed) 
      this.isCompleted = true;
    this.isCompleted = true;
  }

  public orderCompleted() {
    return this.isCompleted;
  }

}
