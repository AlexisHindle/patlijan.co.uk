import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FirebaseProductsService } from "../../services/firebase-products.service";
import { Product } from "../../products.model";

@Component({
  selector: 'app-edit-menu-items',
  templateUrl: './edit-menu-items.component.html',
  styleUrls: ['./edit-menu-items.component.scss']
})
export class EditMenuItemsComponent implements OnInit {
  public products: any;
  constructor(
    public router: Router,
    public productService: FirebaseProductsService
  ) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(data => {
      this.products =  data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as Product;
      });
    })
  }

  public goToEdit(id) {
    this.router.navigate(['admin/edit-item/' + id]);
  }

  public goToAddItem() {
    this.router.navigate(['admin/menu-items']);
  }

}
