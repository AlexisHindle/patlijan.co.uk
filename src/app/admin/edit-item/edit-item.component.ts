import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { FirebaseProductsService } from "../../services/firebase-products.service";
import { Product } from 'src/app/products.model';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {
  singleProduct: any;
  editMenuItem: FormGroup
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private productService: FirebaseProductsService 
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id");
    this.productService.getSingleProduct(id).subscribe(data => {
      this.singleProduct = data;
      console.log(typeof this.singleProduct.price);
    });

    this.editMenuItem = this.formBuilder.group({
      product_id: [],
      name: [''],
      subtitle: [''],
      description: [''],
      details: {
        allergens: [],
      },
      image: [''],
      type: [''],
      arrives: [''],
      ingredients: [''],
      life: [''],
      portions: [''],
      recycle: [''],
      serve: [''],
      store: [''],
      price: [],
      available: [false]
    });
  }

  public onSubmit() {
    this.singleProduct.price = parseInt(this.singleProduct.price); 
    this.productService.updateProduct(this.singleProduct as Product);
  }

}
