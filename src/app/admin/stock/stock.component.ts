import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseProductsService } from "../../services/firebase-products.service";
import { AuthService } from '../../services/auth.service';
import { Product } from "../../products.model";

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  products: Array<any>
  name: string;
  stock: number;

  constructor(
    public router: Router,
    public firebaseProductService: FirebaseProductsService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.firebaseProductService.getProducts().subscribe(data => {
      this.products =  data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as Product;
      });
    })
  }

  public updateStock(stock, product: Product) {
    product.stock = parseInt(stock); 
    this.firebaseProductService.updateProduct(product);
  }

  public logout() {
    this.authService.logout();
    this.router.navigate(['admin/login']).then(() => {
      location.reload();
    });
  }

}
