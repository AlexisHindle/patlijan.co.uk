import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { ProductService } from "../../services/product.service";
import { FirebaseProductsService } from "../../services/firebase-products.service";
import { Product } from "../../products.model";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  @Output()
  basket: EventEmitter<any> = new EventEmitter<any>();
  totalNumberOfProducts: number = 0;
  products: any;
  itemsInBasket: Array<any> = [];
  totalToPay: number;
  itemsInLocalStorage = false;
  stockList: Array<any>;
  stock: number;
  showBasketButton = false;
  localNumberOfItems: number;
  productNumberOfItems: number;
  currentProductStock: Array<any>;
  outOfStockProducts: Array<any>;
  
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public http: HttpClient,
    public productService: ProductService,
    public firebaseProductService: FirebaseProductsService
  ) {}

  ngOnInit(): void {    
    localStorage.removeItem('allProducts');
    const allProducts = JSON.parse(localStorage.getItem('allProducts'));
    if (!allProducts) {
      this.firebaseProductService.getProducts().subscribe(data => {
        const liveProducts = data.map(e => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data() as {}
          } as Product;
        });
        localStorage.setItem('allProducts', JSON.stringify(liveProducts));
        
        if (this.products) {
          const stockDifferences = this.products.filter(item1 => !liveProducts.some(item2 => (item2.stock === item1.stock)))  
          return this.products.map((e1, i) => {
            if (stockDifferences.some(e2 => e2.product_id === e1.product_id)) {  // If there's a match
              this.products[i].stock = liveProducts[i].stock;
            }
          })
        }

      });
    }
    const localProducts = JSON.parse(localStorage.getItem('localProducts'));
    if (localProducts) {   
      this.products = localProducts;
      // TODO: Do we show the basket button in the menu all the time?
      // This is showing products if there is something to show.
      // i.e hasn't just loaded products and something has been added to basket
      // This is checking if basket buttons should display under each section
      this.products.forEach(element => {
        if(element.numberOfItems > 0) {
          this.showBasketButton = true;
        }
      });
    } else {
      this.getAllProducts();
    }
  }

  public getAllProducts() {
    this.firebaseProductService.getProducts().subscribe(data => {
      this.products = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as Product;
      });
      
      localStorage.setItem('allProducts', JSON.stringify(this.products));
      const localProducts = JSON.parse(localStorage.getItem('localProducts'));
      if (!localProducts) {
        localStorage.setItem('localProducts', JSON.stringify(this.products));
      } 
    });
  }

  public viewProduct(event, item) {
    // TODO: Look at setting allProducts in localstorage here so that we can view details of items if not items selected
    // If we do this might look at make other places where allProduct are got to update...
    this.router.navigate(['/item-details', item.product_id]);
  }

  public addSingularItem(item) {
    this.products.forEach((element, index) => {
        if (element.product_id === item.product_id) {
          this.products[index].numberOfItems = item.numberOfItems + 1;
          this.showBasketButton = true;
        };
    });
    localStorage.setItem("localProducts", JSON.stringify(this.products));
  }

  // TODO: need to look if we need this
  public itemAdd(array, item) {
    const itemList = [];
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === item.id) {
        array[i].numberOfItems = itemList.push(item);
      }
    }
  }

  public removeSingularItem(item) {
    const itemToUse = this.products.find(x => x.product_id === item.product_id);
    this.products.forEach((element, index) => {
      if(element.product_id === item.product_id) {
        this.products[index].numberOfItems = itemToUse.numberOfItems > 0 ? itemToUse.numberOfItems - 1 : null;
      }
    });
    localStorage.setItem('localProducts', JSON.stringify(this.products));
  }
  
  public addPrice(itemsToFilter) {
    const prices = itemsToFilter.map(x => {
      return x.price
    });
    this.totalToPay = prices.reduce(function(a, b){
        return a + b;
    }, 0);
  }

  public addToBasket() {
    // TODO: IS THIS BEING USED
    localStorage.setItem("itemsInBasket", JSON.stringify(this.itemsInBasket));
    localStorage.setItem("allProducts", JSON.stringify(this.products));
    this.router.navigate(['/basket']);
  }

  public emptyBasket() {
    localStorage.clear();
  }

}
