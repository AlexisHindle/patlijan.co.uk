import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { FirebaseProductsService } from '../../services/firebase-products.service';
import { Product } from '../../products.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  itemsInBasket: Event;
  constructor(
    public productService: ProductService,
    public firebaseProductsService: FirebaseProductsService
  ) { }

  ngOnInit(): void {}
  
  public getBasketInfo(event: Event) {
    setTimeout(() => {
      this.itemsInBasket = event;
    }, 0);
  }
}
