import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";

import { ProductService } from "../../services/product.service";
import { FirebaseProductsService } from "../../services/firebase-products.service";
import { Product } from "../../products.model";

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {
  actualProduct: any;
  productCountPlaceholder: number;
  inBasket: any;
  productData: any;
  allProducts: any;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    public firebaseProductService: FirebaseProductsService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const localProducts = JSON.parse(localStorage.getItem("localProducts"));
      this.allProducts = localProducts;
      this.productData = localProducts.find(product => product.product_id === Number(params.id));
      this.productCountPlaceholder = this.productData.numberOfItems > 0 ? this.productData.numberOfItems : 0;
      })
      // TODO: Do we get products from localStorage or service?
      // TODO: Need to call service for item details as otherwise this will call localstorage.
  }

  public addToOrder(item) {
    const itemToUse = this.allProducts.find(x => x.product_id === item.product_id);
    this.allProducts.forEach((element, index) => {
      if(element.product_id === item.product_id) {
        this.allProducts[index].numberOfItems = itemToUse.numberOfItems > 0 ? (itemToUse.numberOfItems + 1) : item.numberOfItems + 1;
        this.productCountPlaceholder = this.allProducts[index].numberOfItems;
      }
    });
    localStorage.setItem('localProducts', JSON.stringify(this.allProducts));
  }

  public removeFromOrder(item) {
    const itemToUse = this.allProducts.find(x => x.product_id === item.product_id);
    this.allProducts.forEach((element, index) => {
      if(element.product_id === item.product_id) {
        this.allProducts[index].numberOfItems = itemToUse.numberOfItems > 0 ? (itemToUse.numberOfItems - 1) : item.numberOfItems - 1;
        this.productCountPlaceholder = this.allProducts[index].numberOfItems;
      }
    });
    localStorage.setItem('localProducts', JSON.stringify(this.allProducts));
  }

}
