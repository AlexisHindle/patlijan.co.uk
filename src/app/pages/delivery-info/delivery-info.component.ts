import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";

import { FirebaseProductsService } from '../../services/firebase-products.service';

@Component({
  selector: 'app-delivery-info',
  templateUrl: './delivery-info.component.html',
  styleUrls: ['./delivery-info.component.scss']
})
export class DeliveryInfoComponent implements OnInit {
  
  public info;
  // TODO: Change name of form as necessary
  public registerForm: FormGroup;
  public submitted = false;
  public validForm = false;
  public clientName: string;
  public clientEmail: string; 
  public formData: any;
 
  constructor(
    public firebaseProductService: FirebaseProductsService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      // TODO: Might remove title
      title: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      street: ['', Validators.required],
      houseNumber: ['', Validators.required],
      postcode: ['', [Validators.required, this.validatePostcode]],
      phoneNumber: ['', Validators.required]
    })

    this.info = this.registerForm.controls;
  }

  get form() {
    return this.registerForm.controls;
  }

  public validatePostcode(control: FormControl) {
    let postcode = control.value;
    const arrayOfPostCodes = ["RG7", "RG8", "RG14", "RG18", "RG19", "RG20", "RG26"];
    const postcodeArray = arrayOfPostCodes.filter(x => {
      return new RegExp(x.toLowerCase()).test(postcode.toLowerCase());
    });

    if (postcodeArray.length <= 0) {
      return {
        postcode: {
          parsedPostcode: postcode
        }
      }
    };
    return null;
  }
  
  public onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.validForm = false;
      return;
    } 
    else {
      this.validForm = true;
      this.clientName = `${ this.registerForm.value.firstName } ${ this.registerForm.value.lastName }`;
      this.clientEmail = this.registerForm.value.email;
      this.formData = this.registerForm.value;   
    }
  }

  public onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

  public goHome() {
    this.router.navigate(['/']);
  }

}
