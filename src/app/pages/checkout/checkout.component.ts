import { Component, OnInit, Input } from '@angular/core';
import { Stripe } from 'stripe'

declare var StripeCheckout: StripeCheckoutStatic;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  @Input() amount;
  @Input() description;

  handler: Stripe;

  confirmation: any;
  loading = false;
  constructor() { }

  // TODO: CHECK THAT THIS CAN BE DELETED

  ngOnInit(): void {
    // this.handler = StripeCheckout.configure({
    //   key: 'pk_test_your_key',
    //   image: '/your-avatar.png',
    //   locale: 'auto',
    //   source: async (source) => {
    //     this.loading = true;
    //     const user = await this.auth.getUser();
    //     const fun = this.functions.httpsCallable('stripeCreateCharge');
    //     this.confirmation = await fun({ source: source.id, uid: user.uid, amount: this.amount }).toPromise();
    //     this.loading = false;

    //   }
    // });
  }

}
