import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
  itemsInBasket: any[];
  allProducts: any[];
  mainsTotal: any[];
  sidesTotal: any[];
  desertTotal: any[];
  total: number;
  constructor(
    public router: Router
  ) { }

  // TODO: THING TO LOOK AT
  // Add timestamp to order object
  // Messaging for if purchase accepted, declined and added to basket - is this a component?
  // Look into email if sucess - need to create one for customer and one for company
  // Look into newsletter sign up - mailchimp?
 

  ngOnInit(): void {
    this.allProducts = JSON.parse(localStorage.getItem("localProducts"));
    this.itemsInBasket = this.allProducts.filter(x => {
      return x.numberOfItems > 0;
    });

    this.mainsTotal = this.itemsInBasket.filter(x => x.type === 'main');
    this.sidesTotal = this.itemsInBasket.filter(x => x.type === 'starter');
    this.desertTotal = this.itemsInBasket.filter(x => x.type === 'desert');

    let num = 0
    const total = this.itemsInBasket.forEach(item => {
      // TODO: Just to be clear this is products with number of items > 0
      num = num + item.numberOfItems * item.price;
      return num;
    });
    this.total = num
  }

  // TODO: Not used but nice to have and keep if needed later
  public removeDuplicates(array, key) {
    let lookup = {};
    array.forEach(element => {
      lookup[element[key]] = element
    });
    return Object.keys(lookup).map(key => lookup[key]);
  };

  public goToOrder() {
    this.router.navigate(['/']);
  }

  public backToOrder() {
    this.router.navigate(['/']);
  }

  public goToDeliveryDetails() {
    this.router.navigate(['delivery-info']);
  }
 
}
