import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Inject, Input, SimpleChange, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { PaymentService } from '../../services/payment.service';
import { FirebaseProductsService } from '../../services/firebase-products.service';
import { FirebaseOrdersService } from '../../services/firebase-orders.service';
import { OrderNumber } from "../../products.model";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  @Input()
  public info;
  @Input()
  public clientName;
  @Input()
  public clientEmail;
  @Input()
  public validForm;
  @ViewChild('cardNumberEl') cardNumberEl: ElementRef;
  @ViewChild('cardCVCEl') cardCVCEl: ElementRef;
  @ViewChild('cardExpiryEl') cardExpiryEl: ElementRef;
  
  public itemsInBasket: Array<any> = [];
  public filteredItems: Array<any> = [];
  public card: any;
  public cardHandler = this.onChange.bind(this);
  public cardError;
  public client_secret: any;
  public price: number;
  public emailBody;
  public orderNumber: any;
  public emailProducts: any;
  private _subscription: Subscription;
  
  public paymentForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public paymentService: PaymentService,
    private cd: ChangeDetectorRef,
    private router: Router,
    public firebaseProductService: FirebaseProductsService,
    public firebaseOrdersService: FirebaseOrdersService
  ) { }

  ngOnDestroy() {
    // We remove event listener here to keep memory clean
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();

  }

  ngAfterViewInit() {
    if (!this.card) {
      this.initiateCardElement();
    }
  }

  ngOnInit(): void {
    // TODO: Need to add name, etc to stripe payment_details
    this.itemsInBasket = JSON.parse(localStorage.getItem("localProducts"));
    if (this.itemsInBasket) {
      const filteredItems = this.itemsInBasket.filter(x => x.numberOfItems > 0);
      localStorage.setItem('emailProducts',JSON.stringify(filteredItems));
      this.emailProducts = JSON.parse(localStorage.getItem('emailProducts'));
      const arrayOfPrices = [];
      filteredItems.forEach(element => {
        arrayOfPrices.push(element.price * element.numberOfItems);
      });
      let num =  0;
      arrayOfPrices.forEach(item => {
        num = num + item;
        return num;
      });
      this.price = num * 100;
      this.paymentService.createIntent(this.price, this.clientEmail).pipe().subscribe(data => {
        this.client_secret = data.client_secret;
      })
    }
    // TODO: Not sure if the form needs to be created like this
    this.paymentForm = this.formBuilder.group({});
  }

  initiateCardElement() {
    // Giving a base style here, but most of the style is in scss file
      const style = {
        base: {
          color: '#32325d',
          lineHeight: '24px',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#000',
            fontWeight: '700'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };
      const elements = stripe.elements();
      this.card = elements.create('cardNumber', {
        style: style,
        placeholder: 'Card Number'
      });
      this.card.mount(this.cardNumberEl.nativeElement);
     
      this.card = elements.create('cardExpiry', {
        style: style,
        placeholder: 'Card Expiry'
      });
      this.card.mount(this.cardExpiryEl.nativeElement);
  
      this.card = elements.create('cardCvc', {
        style: style,
        placeholder: 'Card CVC'
      });
      this.card.mount(this.cardCVCEl.nativeElement);

      this.card.addEventListener('change', this.cardHandler);
  }

  onChange({error}) {
      if (error) {
          this.cardError = error.message;
      } else {
          this.cardError = null;
      }
      this.cd.detectChanges();
  }

  async createStripeToken() {
      const {token, error} = await stripe.createToken(this.card);
      if (token) {
          this.onSuccess(token);
      } else {
          this.onError(error);
      }
  }

  // TODO: Might need this at a later date
  public addStock(item) {
    // this.stockList.forEach(stockItem => {
    //   if (stockItem.product_id === item.id) {
    //     let stock = 0;
    //     stock = stockItem.stock;
    //     stockItem.stock = stock + 1;
    //     this.firebaseProductService.updateProduct(stockItem);
    //   }
    // })
  }

  public removeStock() {
    this.itemsInBasket.forEach(items => {
      if (items.numberOfItems > 0) {
        let stock = 0;
        stock = items.stock
        items.stock = stock - items.numberOfItems;
        items.numberOfItems = 0;
        this.firebaseProductService.updateProduct(items);
      }
    })
  }

  onSuccess(token) {
    this.cardError = "";
    this.paymentService.createIntent(this.price, this.clientEmail).pipe().subscribe();
    stripe.confirmCardPayment(this.client_secret, {
      receipt_email: this.clientEmail,
      payment_method: {
        card: this.card,
        billing_details: {
          name: this.clientName,
          email: this.clientEmail
        },
      },
    })
    .then(result => {
      if (result.error) {
        // Show error to your customer (e.g., insufficient funds)
        console.log(result.error.message);
        this.onError(result.error);
      } else {
        // The payment has been processed!
        if (result.paymentIntent.status === 'succeeded') {
          this._subscription = this.firebaseProductService.getOrderNumber().subscribe(data => {
            this.orderNumber = data.map(e => {
              return {
                id: e.payload.doc.id,
                ...e.payload.doc.data() as {}
              } as OrderNumber;
            });
            this.createOrder(this.orderNumber[0].orderNumber);
            this.sendEmail(this.orderNumber[0].orderNumber);
          }); 
          this.removeStock()
          localStorage.clear();
          // this._subscription.unsubscribe();
          this.router.navigate(['/confirmation']);
          // Show a success message to your customer
          // There's a risk of the customer closing the window before callback
          // execution. Set up a webhook or plugin to listen for the
          // payment_intent.succeeded event that handles any business critical
          // post-payment actions.
        }
      }
    });
  }

  public createOrder(orderNumber) {
    let sum = 0;
    this.emailProducts.forEach(element => {
      sum = sum + (element.price * element.numberOfItems)
    });

    const todayDate = new Date();
    const dateToString = todayDate.toString();

    const productToOrder = {
      dateOrdered: dateToString,
      orderNumber: orderNumber,
      title: this.info.title,
      nameOfClient: `${ this.info.firstName } ${ this.info.lastName }`,
      address: `${ this.info.houseNumber } ${ this.info.street }, ${ this.info.postcode }`,
      email: `${ this.info.email }`,
      phoneNumber: `${ this.info.phoneNumber }`,
      itemsOrdered: this.emailProducts,
      amount: sum,
      completed: false
    }
    this.firebaseOrdersService.createOrder(productToOrder);
  }

  onError(error) {
      if (error.message) {
          this.cardError = error.message;
      }
  }

    // TODO: Create sendEmail for customer and company
  // Maybe put send to customer in promise so that if company email is sent then send to customer
  // This makes sure that company definitely receives order
  public sendEmail(orderNumber) {
    // TODO: Make sure this is pulling right data
    // TODO: add order number, time and date
    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, '0');
    const mm = String(todayDate.getMonth()).padStart(2, '0');
    const yyyy = todayDate.getFullYear();

    this.emailBody = `<h1>New Order from: ${ this.info.firstName } ${ this.info.lastName }</h1>`;
    this.emailBody += `<h1>Order No: ${ orderNumber }</h1>`;
    this.emailBody += `<h2>Ordered on: ${ todayDate }</h2>`;
    this.emailBody += `<h2>They ordered:</h2>`;
    let sum = 0;
    this.emailProducts.forEach(element => {
      this.emailBody += `<h2>${ element.numberOfItems } X ${ element.name } - £${ element.price * element.numberOfItems }.00</h2>`;
      sum = sum + (element.price * element.numberOfItems)
    });
    this.emailBody += `<h2>Total paid: £${ sum }.00</h2>`;
    this.emailBody += `<h2>Their address is: ${ this.info.houseNumber } ${ this.info.street }, ${ this.info.postcode }</h2>`;
    this.emailBody += `<h2>You can contact them by phone on: ${ this.info.phoneNumber }</h2>`;
    this.emailBody += `<h2>Or by email on: ${ this.info.email }</h2>`

    Email.send({
      Host: 'smtp.elasticemail.com',
      Username: 'alexishindle@gmail.com',
      Password: '80F61A79429993A23F8DA59251ACA8EE6344',
      Port: 2525,
      To: ['hello@patlijan.co.uk'],
      From: this.info.email,
      Subject: 'Your order from Patlijan',
      Body: this.emailBody
    }).then(message => {
      console.log('This order was sent', message);
      this.firebaseProductService.increaseCount(this.orderNumber);
      this._subscription.unsubscribe();
    })
  }
}


