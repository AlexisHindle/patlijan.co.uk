import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() basket;
  totalNumberOfItems: number;
  showMobileMenu = false;
  showBasketButton = false;
  paramsHasId: number;
  isLogin = false;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    const hasUser = JSON.parse(localStorage.getItem('user'));
    // TODO confirm if we need anymore checks - eg: window.location.pathname === '/admin/stock' && 
    if (hasUser) {
      this.isLogin = true;
    }
    this.basket = JSON.parse(localStorage.getItem('localProducts'));
    this.paramsHasId = Number(this.route.snapshot.params.id);
    if(this.paramsHasId > 0)
      this.showBasketButton = true;
    if(this.router.url === "/" && this.basket) 
      this.showBasketButton = true;
  }
  
  ngAfterViewInit() {
   this.updateButtonWithBasketInfo();
  }

  public logout() {
    this.authService.logout();
    this.router.navigate(['admin/login']).then(() => {
      location.reload();
    });
  }

  public updateButtonWithBasketInfo(){
    let num = 0;
    setTimeout(() => {
      if((this.router.url === "/" && this.basket) || this.paramsHasId > 0) {
        // this.totalNumberOfItems = this.basket.length;
        this.basket.forEach(element => {
          num = num + element.numberOfItems;
          return num;
        });
      }
      this.totalNumberOfItems = num;
    }, 0)
  }

  public goToBasket() {
    this.router.navigate(['/basket']);
  }

  public mobileMenuIsActive() {
    this.showMobileMenu = true;
  }

  public closeMobileNav() {
    this.showMobileMenu = false;
  }

  public closeAllAndGoToBasket() {
    this.goToBasket();
    this.closeMobileNav();
  }

  public goToStock() {
    this.router.navigate(['admin/stock']);
    this.closeMobileNav();
  }

  public goToMenu() {
    this.router.navigate(['admin/menu-edit']);
    this.closeMobileNav();
  }

  public goToOrders() {
    this.router.navigate(['admin/orders']);
    this.closeMobileNav();
  }

}
