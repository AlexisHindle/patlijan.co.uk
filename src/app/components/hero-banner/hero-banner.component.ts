import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-hero-banner',
  templateUrl: './hero-banner.component.html',
  styleUrls: ['./hero-banner.component.scss']
})
export class HeroBannerComponent implements OnInit {
  @Input()
  title;

  @Input()
  content;
  
  constructor() {}

  ngOnInit(): void {}

}
